package com.smarteinc.assignment.booking.exception;

/**
 * YS: These error message can take from properties file or YML depends upon the technology 
 */
public final class ErrorMessage
{
	public static final String TICKET_DATA_NOT_NULL = "Ticket data should not be null ";
	public static final String START_DATE_NOT_NULL = "Ticket data - Start Date should not be null ";
	public static final String END_DATA_NOT_NULL = "Ticket data - End Date should not be null";
	public static final String DESTINATION_NOT_NULL = "Ticket data - Destination should not be null";
	public static final String FROM_NOT_NULL = "Ticket data - From should not be null";
	public static final String PASSENGER_DATA_NOT_NULL = "Passenger data should not be null ";
	public static final String NAME_NOT_NULL = "Passenger name should not be null";
	public static final String SUPPORTED_TYPE = "Only type 1 and 2 tickets are supported";
	public static final String TYPE_NOT_NULL = "Ticket data - Type should not be null";
}
