package com.smarteinc.assignment.booking.exception;

public class ValidationException extends BaseException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public ValidationException(String errorMessage, Throwable cause, int returnCode)
	{
		super(cause);
		setErrorCode(returnCode);
		setErrorMessage(errorMessage);
		setCause(cause);

	}

	public ValidationException(String errorMessage, int returnCode)
	{
		setErrorCode(returnCode);
		setErrorMessage(errorMessage);
	}
	public ValidationException(String errorMessage)
	{
		setErrorMessage(errorMessage);
	}

}
