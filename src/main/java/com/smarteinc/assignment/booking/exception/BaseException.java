package com.smarteinc.assignment.booking.exception;

public class BaseException extends RuntimeException
{

	private static final long serialVersionUID = 1L;

	public BaseException(Throwable cause)
	{
		super(cause);
	}

	public BaseException()
	{

	}

	private final ErrorResponse errorResponse = new ErrorResponse();

	public void setErrorCode(Integer errorCode)
	{
		this.errorResponse.setErrorCode(errorCode);
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorResponse.setErrorMessage(errorMessage);
	}

	public void setCause(Throwable cause)
	{
		this.errorResponse.setCause(cause);
	}

	public ErrorResponse getErrorResponse()
	{
		return errorResponse;
	}

}
