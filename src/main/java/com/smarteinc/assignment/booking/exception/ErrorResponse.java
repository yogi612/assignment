package com.smarteinc.assignment.booking.exception;

import java.io.Serializable;

public class ErrorResponse implements Serializable
{
	private static final long serialVersionUID = -7635128853462319333L;

	private int errorCode;

	private String errorMessage;

	private Throwable cause;

	public int getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public Throwable getCause()
	{
		return cause;
	}

	public void setCause(Throwable cause)
	{
		this.cause = cause;
	}

}
