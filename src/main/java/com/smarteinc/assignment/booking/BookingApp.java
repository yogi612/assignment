package com.smarteinc.assignment.booking;

import java.time.LocalDate;
import java.util.Optional;

import com.smarteinc.assignment.booking.constants.ApplicationConstants;
import com.smarteinc.assignment.booking.exception.ErrorMessage;
import com.smarteinc.assignment.booking.exception.ValidationException;

public class BookingApp
{

	public int bookingProcess(Ticket x)
	{
		validateAllFieldsPresent(x);
		if (x.getType() == ApplicationConstants.BUS_TICKET)
		{
			bookBusTicket(x);
		}
		else
		{
			//YS; considered the default booking because validation taken care the other values.
			bookCarTicket(x);
		}
		//YS: this should be call on validation success otherwise not.
		sendTicketBookedMail(x);
		//method should return the type of ticket booked
		return x.getType();
	}

	private void bookCarTicket(Ticket x)
	{
		//assume Car is booked by making some entries in db
	}

	private void bookBusTicket(Ticket x)
	{
		//assume Bus is booked by making some entries in db
	}

	private void sendTicketBookedMail(Ticket x)
	{
		//Assume email is sent to passenger that his/her ticket is booked
	}

	//Ensure all input data is present
	//TODO: YS: Need to move in Separate class which we can called BusinessRule - 
	//It should be Generic Validation so that in future anyone can easily extend or can easily add validation method.
	private void validateAllFieldsPresent(Ticket ticket)
	{
		Optional<Ticket> ticketOpt = Optional.ofNullable(ticket);
		StringBuilder errorMessageBuilder = new StringBuilder();
		if (!ticketOpt.isPresent())
		{
			errorMessageBuilder.append(ErrorMessage.TICKET_DATA_NOT_NULL);
		}
		else
		{
			validateDate(ticket.getStartDate(), errorMessageBuilder, ErrorMessage.START_DATE_NOT_NULL);
			validateDate(ticket.getEndDate(), errorMessageBuilder, ErrorMessage.END_DATA_NOT_NULL);
			validateData(ticket.getFrom(), errorMessageBuilder, ErrorMessage.FROM_NOT_NULL);
			validateData(ticket.getDestination(), errorMessageBuilder, ErrorMessage.FROM_NOT_NULL);

			Optional<Passenger> passengerOpt = Optional.ofNullable(ticket.getPassenger());
			if (!passengerOpt.isPresent())
			{
				errorMessageBuilder.append(ErrorMessage.PASSENGER_DATA_NOT_NULL);
			}
			else
			{
				//TODO: YS: We can easily add more validation on passenger data.
				validateData(ticket.getPassenger().getName(), errorMessageBuilder, ErrorMessage.NAME_NOT_NULL);
			}
			if (!ApplicationConstants.getTicketTypeList().contains(ticket.getType()))
			{
				throw new IllegalArgumentException(ErrorMessage.SUPPORTED_TYPE);
			}
		}
		toCheckErrorMessageBuilder(errorMessageBuilder);

	}

	private void validateData(String data, StringBuilder builder, String errorMessage)
	{
		Optional<String> dataOpt = Optional.ofNullable(data);

		//TODO: YS: We can add date formatting validation.
		if (!dataOpt.isPresent())
		{
			builder.append(errorMessage);
		}
	}

	private void validateDate(LocalDate localDate, StringBuilder builder, String errorMessage)
	{
		Optional<LocalDate> localDateOpt = Optional.ofNullable(localDate);

		//TODO: YS: We can add date formatting validation.
		if (!localDateOpt.isPresent())
		{
			builder.append(errorMessage);
		}
	}

	private void toCheckErrorMessageBuilder(StringBuilder errorMessageBuilder)
	{
		if (errorMessageBuilder.length() != 0)
		{
			//TODO: YS: We can set the error code as well if required
			throw new ValidationException(errorMessageBuilder.toString());
		}
	}

}
