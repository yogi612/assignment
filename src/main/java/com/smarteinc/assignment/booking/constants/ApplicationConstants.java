package com.smarteinc.assignment.booking.constants;

import java.util.Arrays;
import java.util.List;

public final class ApplicationConstants
{
	private static final List<Integer> ticketTypeList = Arrays.asList(1, 2);
	
	public static int BUS_TICKET = 1, CAR_TICKET = 2;

	public static List<Integer> getTicketTypeList()
	{
		return ticketTypeList;
	}
}
